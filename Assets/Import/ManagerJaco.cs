using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ManagerJaco : Singleton<ManagerJaco>
{
    public List<TirachinasJaco> tira = new List<TirachinasJaco>();
   
    public TirachinasJaco GetTirachinasJaco(int index)
    {
        return tira[index];
    }

}
