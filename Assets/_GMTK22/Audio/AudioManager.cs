using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public List<AudioClip> normalCollision = new List<AudioClip>();
    public List<AudioClip> goodChimes = new List<AudioClip>();
    public List<AudioClip> badChimes = new List<AudioClip>();
    public AnimationCurve diceCollisionSoundCurve;
    public float maxDiceCollisionSoundCurve;

    public List<AudioClip> audioResult = new List<AudioClip>();

    public AudioClip bso;
    public bool activeBSO;
    private AudioSource bso_audioSource;

    private void Awake() {
        base.Awake();

        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        if (activeBSO) {
            GameObject auGO = new GameObject();
            AudioSource au = auGO.AddComponent<AudioSource>();
            bso_audioSource = au;
            au.clip = bso;
            au.loop = true;
            au.volume =1f;
            au.Play();
            DontDestroyOnLoad(auGO);
        }
    }

    public void DiceColision(float value = 5) {
        GameObject auGO = new GameObject();
        AudioSource au = auGO.AddComponent<AudioSource>();
        au.clip = normalCollision[Random.Range(0, normalCollision.Count)];
        au.volume = diceCollisionSoundCurve.Evaluate(value)/ maxDiceCollisionSoundCurve;
        au.pitch = Random.Range(1.0f, 1.1f);
        au.Play();
        StartCoroutine(RemoveAudio(au));
    }

    public void FloorColision(float value = 5) {
        GameObject auGO = new GameObject();
        AudioSource au = auGO.AddComponent<AudioSource>();
        au.clip = normalCollision[Random.Range(0, normalCollision.Count)];
        au.volume = diceCollisionSoundCurve.Evaluate(value) / maxDiceCollisionSoundCurve;
        au.pitch = Random.Range(0.9f, 0.95f);
        au.Play();
        StartCoroutine(RemoveAudio(au));
    }

    public void AudioResultTurn(int index) {
        AudioClip clip = null;
        if (index > 0) {
            if (index < 2) {
                clip = goodChimes[0];
            } else if (index < 4) {
                clip = goodChimes[1];
            } else if (index < 8) {
                clip = goodChimes[2];
            } else {
                clip = goodChimes[3];
            }
            AudioSource.PlayClipAtPoint(clip, GM.cam.transform.position, 1);
        }
        if (index < 0) {
            if(index > -4) {
                clip = badChimes[0];
            } else {
                clip = badChimes[1];
            }
            AudioSource.PlayClipAtPoint(clip, GM.cam.transform.position, 1);
        }
        //print(index + ": " + clip);
    }

    public void PlayAtCamera(AudioClip clip) {
        AudioSource.PlayClipAtPoint(clip, GM.cam.transform.position);
    }

    IEnumerator RemoveAudio(AudioSource au) {
        yield return new WaitForSeconds(au.clip.length + 0.5f);
        Destroy(au.gameObject);
    }
}
