using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice6 : Dice
{
    public DiceFace[] faces = {
        new DiceFace(1, Vector3.up),
        new DiceFace(2, Vector3.right),
        new DiceFace(3, Vector3.forward),
        new DiceFace(4, Vector3.back),
        new DiceFace(5, Vector3.left),
        new DiceFace(6, Vector3.down)
    };

    //Deber�a devolver el n�mero entre el 1 y el 6 acorde a la rotaci�n del transform del dado
    public override float GetDiceValue() { 
        float result = 0f;
        float closestDot = -1f;
        foreach (DiceFace face in faces) {
            float dot = Vector3.Dot(transform.TransformVector(face.normal), Vector3.up);
            if ( dot > closestDot) {
                result = face.value;
                closestDot = dot;
            }
        }
        return result;
    }

}
