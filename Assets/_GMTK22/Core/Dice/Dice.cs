using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Dice : MonoBehaviour
{
    public struct DiceFace
    {
        public int value;
        public Vector3 normal;
        public DiceFace(int value, Vector3 normal) {
            this.value = value;
            this.normal = normal;
        }
    }

    [Header("References")]
    public Renderer myRenderer;
    private Rigidbody rb;

    [Header("Parameters")]
    public List<Map.AreaColor> colorByAreas;
    public float MINIMUM_VELOCITY_ISMOVING = 0.1f;
    public float MINIMUM_ANGULAR_VELOCITY_ISMOVING = 0.05f;

    [Header("Variables")]
    public bool isReleased = false;
    public bool isMoving = false;

    //METHODS
    private void Awake() {
        rb = GetComponent<Rigidbody>();
        myRenderer = this.GetComponent<Renderer>();
        GM.Instance.dices.Add(this);
    }
    public virtual void Update() {

        if (isReleased) {
            //Check if the dice is moving
            UpdateIsMoving();
            //Update the dice color and glowing face
            UpdateDiceMaterial();
        }

        if (this.transform.position.y < GM.map.diceKillingHeight) {
            RemoveDice();
        }
    }

    public virtual void Throw() {
        isReleased = true;
    }

    public abstract float GetDiceValue();
    public static float GetTotalDicesValue(List<Dice> dices) {
        float total = 0;
        foreach (Dice d in dices) {
            total += d.GetDiceValue();
        }
        return total;
    }

    public virtual void UpdateIsMoving() {
        bool wasMoving = isMoving;
        isMoving = (rb.velocity.magnitude > MINIMUM_VELOCITY_ISMOVING || rb.angularVelocity.magnitude > MINIMUM_ANGULAR_VELOCITY_ISMOVING);
        if (!isMoving && wasMoving) {
            OnDiceStopMoving();
        }
    }
    public virtual void OnDiceStopMoving() {
        rb.mass = 1;
    }
    public virtual bool IsInsideMapBounds() {
        return GM.map.IsInsideMapBounds(this.transform.position);
    }
    public virtual Map.AreaType GetAreaTypeUnderDice() {
        return GM.map.GetAreaAtPosition(this.transform.position);
    }
    public virtual void UpdateDiceMaterial() {
        //Update the material player color
        if (IsInsideMapBounds()) {
            Map.AreaType myArea = GM.map.GetAreaAtPosition(this.transform.position);
            foreach (Map.AreaColor areaColor in colorByAreas) {
                if (areaColor.area == myArea) {
                    this.GetComponent<Renderer>().material.SetColor("_PlayerColor", areaColor.color);
                }
            }
        } else {
            this.GetComponent<Renderer>().material.SetColor("_PlayerColor", Color.black);
        }
        //Make the correct dice face glow
        myRenderer.material.SetFloat("_DiceValue", GetDiceValue());

    }

    public bool IsDiceStationary() {
        return (!isMoving && isReleased && IsInsideMapBounds());
    }
    public bool IsMoving() {
        return !IsDiceStationary();
    }

    void RemoveDice() {
        GM.Instance.dices.Remove(this);
        transform.DOScale(Vector3.zero, 4).SetEase(Ease.OutCirc).OnComplete(() => {
            Destroy(gameObject);
        });

    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Floor")) {
            AudioManager.Instance.FloorColision(rb.velocity.magnitude);
        } else if (collision.gameObject.CompareTag("Dice")) {
            AudioManager.Instance.DiceColision(rb.velocity.magnitude);
            GM.Instance.CameraShake(rb.velocity.magnitude/500);
        }
    }
}
