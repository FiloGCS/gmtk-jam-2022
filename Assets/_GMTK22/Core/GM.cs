using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public enum GameMode { Menu, GameTwoControllers, GameOneController }
public enum GameState { RegularTurn, Comeback, End }
public enum Player { P1, P2, Count, Null }


public class GM : Singleton<GM>
{

    public bool workingInProject;
    public static Camera cam;
    public static Map map;
    public AudioManager audioManager;
    public List<Launcher> launchers = new List<Launcher>();
    public Launcher currentLauncher;
    public List<Dice> dices = new List<Dice>();

    public GameMode gameMode;
    public Player currentPlayer;
    public EventSystem eventSystem;
    [Header("Interfaz Menu")]
    public GameObject canvasMenu;
    public GameObject canvasPause;
    public GameObject buttonDefaultMenu;
    public GameObject buttonDefaultPause;
    public Image fade;
    public GameObject cameraFocus;
    public CanvasGroup titleGame;
    public CanvasGroup canvasGroupButtonsMenu;
    public CanvasGroup byOurs;
    [Header("Imagenes controles")]
    public GameObject controller_share;
    public GameObject controller_grey;
    public GameObject controller_1;
    public GameObject controller_2;
    public GameObject keyboard_share;
    public GameObject keyboard_1;
    public GameObject keyboard_2;
    [Header("Interfaz juego")]
    public GameObject canvasJuego;
    public TextMeshProUGUI textMessage;
    public TextMeshProUGUI SubtextMessage;
    private Coroutine coDetectMovement;
    public bool isPlaying;
    public List<PlayerController> playerControllers = new List<PlayerController>();
    private bool letClickInterface;
    public Color blueColor;
    public Color redColor;
    private bool isBackingToMenu;

    private float pointsStartTurn = 0;
    private float pointsEndTurn;

    public AnimationTutorial animTutorial_P1;
    public AnimationTutorial animTutorial_P2;

    public GameState state = GameState.RegularTurn;
    public bool waitingForDiceToStop = false;

    private new void Awake() {
        base.Awake();

        //Application Setup
        Application.targetFrameRate = 60;

        //Load References
        map = FindObjectOfType<Map>();
        audioManager = FindObjectOfType<AudioManager>();
        cam = Camera.main;

        InitMenu();
        canvasJuego.SetActive(false);
        currentPlayer = Player.Null;
        //TODO - Needed??
        InputSystem.EnableDevice(Keyboard.current);
        InputSystem.EnableDevice(Mouse.current);

        TryFindControllers();
    }

    void InitMenu() {
        InitVisualMenu();
        eventSystem.SetSelectedGameObject(buttonDefaultMenu);
        gameMode = GameMode.Menu;
        canvasMenu.SetActive(true);
        UpdateControllerVisuals();
    }
    void InitVisualMenu() {
        if (!workingInProject) {
            if (DontDestroy.Instance.firstGame) {
                StartCoroutine(FadeImage(fade, false, 1, 2));
                StartCoroutine(FadeCanvasGroup(titleGame, true, 1.5f, 2.5f, false));
                StartCoroutine(FadeCanvasGroup(titleGame, false, 1.5f, 5, true));
                //StartCoroutine(FadeCanvasGroup(byOurs,true,1f,2.5f,false));
                //StartCoroutine(FadeCanvasGroup(byOurs,false,1,4,true));                
                cameraFocus.transform.localPosition = new Vector3(0, -30, 0);
                StartCoroutine(FadeCanvasGroup(canvasGroupButtonsMenu, true, 2f, 7, false));
                StartCoroutine(ActivateButtonsMenu(7));
                DontDestroy.Instance.firstGame = false;
            } else {
                titleGame.gameObject.SetActive(false);
                StartCoroutine(FadeImage(fade, false, 1, 2));
                cameraFocus.transform.localPosition = new Vector3(0, -30, 0);
                StartCoroutine(FadeCanvasGroup(canvasGroupButtonsMenu, true, 1.5f, 2, false));
                StartCoroutine(ActivateButtonsMenu(2));
                DontDestroy.Instance.firstGame = false;
                letClickInterface = true;
            }
        } else {
            titleGame.gameObject.SetActive(false);
            StartCoroutine(FadeImage(fade, false, 1, 2));
            cameraFocus.transform.localPosition = new Vector3(0, -30, 0);
            StartCoroutine(FadeCanvasGroup(canvasGroupButtonsMenu, true, 1.5f, 2, false));
            StartCoroutine(ActivateButtonsMenu(2));
            DontDestroy.Instance.firstGame = false;
            letClickInterface = true;
        }
    }

    void TryFindControllers() {
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        if (players.Length > 0) {
            //Add player with index 0 to the playerControllers list
            for (int i = 0; i < players.Length; i++) {
                if (players[i].playerInput.playerIndex == 0) {
                    playerControllers.Add(players[i]);
                }
            }
            //Then add player with index 1 to the playerControllers list
            if (players.Length == 2) {
                for (int i = 0; i < players.Length; i++) {
                    if (players[i].playerInput.playerIndex == 1) {
                        playerControllers.Add(players[i]);
                    }
                }
            }
            //Init all playerControllers
            foreach (PlayerController item in playerControllers) {
                item.InitPlayer();
            }
            UpdateControllerVisuals();
        }
    }
    public void AddPlayerController(PlayerController playerController) {
        playerControllers.Add(playerController);
        UpdateControllerVisuals();
    }



    //UI Coroutines
    IEnumerator FadeImage(Image image, bool fadeIn, float time, float wait) {
        if (fadeIn) {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
            yield return new WaitForSeconds(wait);
            while (image.color.a < 1) {
                image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + Time.deltaTime / time);
                yield return null;
            }
        } else {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
            yield return new WaitForSeconds(wait);
            while (image.color.a > 0) {
                image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - Time.deltaTime / time);
                yield return null;
            }
        }
    }
    IEnumerator ActivateButtonsMenu(float time) {
        yield return new WaitForSeconds(time);
        letClickInterface = true;
    }
    IEnumerator FadeCanvasGroup(CanvasGroup image, bool fadeIn, float time, float wait, bool waitStart) {
        if (waitStart)
            yield return new WaitForSeconds(wait);
        if (fadeIn) {
            image.alpha = 0;
            image.gameObject.SetActive(true);
            if (!waitStart)
                yield return new WaitForSeconds(wait);
            while (image.alpha < 1) {
                image.alpha = image.alpha + Time.deltaTime / time;
                yield return null;
            }
        } else {
            image.alpha = 1;
            if (!waitStart)
                yield return new WaitForSeconds(wait);
            while (image.alpha > 0) {
                image.alpha = image.alpha - Time.deltaTime / time;
                yield return null;
            }
            image.gameObject.SetActive(false);
        }
    }

    //Game Pause
    public void PauseGame() {
        //Disable pause in menus
        if (gameMode == GameMode.Menu || !isPlaying || isBackingToMenu) {
            return;
        }
        //Set InputActions to "UI"
        foreach (PlayerController p in playerControllers) {
            p.playerInput.SwitchCurrentActionMap("UI");
        }
        //UI
        canvasPause.SetActive(true);
        canvasPause.GetComponent<CanvasGroup>().alpha = 0;
        eventSystem.SetSelectedGameObject(buttonDefaultPause);
        StartCoroutine(FadeCanvasGroup(canvasPause.GetComponent<CanvasGroup>(), true, 0.1f, 0, false));
    }
    public void ResumeGame() {
        //Set InputActions to "Player"
        foreach (PlayerController p in playerControllers) {
            p.playerInput.SwitchCurrentActionMap("Player");
        }
        //UI
        StartCoroutine(FadeCanvasGroup(canvasPause.GetComponent<CanvasGroup>(), false, 0.1f, 0, false));
    }

    public void ChangeControls() {
        //Destroy all existing PlayerControllers
        foreach (PlayerController p in playerControllers) {
            Destroy(p.gameObject);
        }
        playerControllers.Clear();
        UpdateControllerVisuals();
    }

    public static Launcher GetCurrentLauncher() {
        if ((int)Instance.currentPlayer < Instance.launchers.Count) {
            return Instance.launchers[(int)Instance.currentPlayer];
        } else {
            return null;
        }
    }
    public static Launcher GetLauncher(PlayerController playerController) {
        if (Instance.gameMode == GameMode.GameOneController) {
            return GetCurrentLauncher();
        } else {
            return Instance.launchers[playerController.playerInput.playerIndex];
        }
    }
    public static Launcher GetLauncher(Player player) {
        return Instance.launchers[(int)player];
    }

    public void LaunchGame() {
        //If there aren't enough players
        if (playerControllers.Count < 1 || !letClickInterface) {
            return;
        }
        //TODO - Hiding the UI �?
        textMessage.text = "";
        SubtextMessage.text = "";
        textMessage.transform.parent.GetComponent<CanvasGroup>().alpha = 0;
        StartCoroutine(FadeCanvasGroup(canvasGroupButtonsMenu, false, 1f, 0, false));
        //Set the game mode depending on the number of controllers
        if (playerControllers.Count == 2) {
            gameMode = GameMode.GameTwoControllers;
        } else if (playerControllers.Count == 1) {
            gameMode = GameMode.GameOneController;
        }

        //Animate the camera moving towards the table, then Load the Game
        cameraFocus.transform.DOLocalMoveY(0, 3).SetEase(Ease.InOutSine).OnComplete(() => {
            StartGame();
        });
    }
    public void StartGame() {
        //Start the game
        isPlaying = true;

        if (gameMode == GameMode.GameTwoControllers) {
            if (launchers[0].playerController != null) {
                launchers[0].playerController.enabled = true;
                launchers[0].playerController.playerInput.enabled = true;
                launchers[0].playerController.playerInput.SwitchCurrentActionMap("Player");
            }

            if (launchers[1].playerController != null) {
                launchers[1].playerController.enabled = true;
                launchers[1].playerController.playerInput.enabled = true;
                launchers[1].playerController.playerInput.SwitchCurrentActionMap("Player");
            }
        }
        SelectFirstPlayer();

        //Show the ingame UI
        canvasJuego.SetActive(true);
    }

    private void Update() {
        if (isPlaying) {
            bool diceStillMoving = false;
            foreach (Dice d in dices) {
                if (d.IsMoving()) {
                    diceStillMoving = true;
                    break;
                }
            }
            //Once all the dice stopped moving
            if (!diceStillMoving) {
                GameState nextState = CheckNextState();

                if (nextState == GameState.RegularTurn) {
                    if (state == GameState.Comeback) {
                        //COMEBACK SUCCESS
                        ShowComebackSuccessMessage();
                    }
                    NextTurn();
                } else if (nextState == GameState.Comeback) {
                    //COMEBACK
                    ShowComebackChanceMessage();
                    NextTurn();
                } else if (nextState == GameState.End) {
                    isPlaying = false;
                    if (state == GameState.Comeback) {
                        //COMEBACK FAILED
                        ShowComebackFailedMessage();
                    } else {
                        //JINXED
                        ShowJinxedMessage();
                    }
                }
                state = nextState;
            }
        }
    }

    public GameState CheckNextState() {
        //By default the next step is to give the turn to the next player
        GameState nextState;

        //Check if any player has the MAX SCORE DIFFERENCE
        float scoreDelta = GetPlayer2Score() - GM.Instance.GetPlayer1Score();
        if (Mathf.Abs(scoreDelta) >= ScoreManager.Instance.MAX_SCORE_DIFFERENCE) {
            if (scoreDelta > 0) {
                //If player 2 is the one ahead by more than 10
                if (currentPlayer == Player.P2) {
                    nextState = GameState.Comeback;
                } else {
                    nextState = GameState.End;
                }
            } else {
                //If player 1 is ahead by more than 10
                if (currentPlayer == Player.P1) {
                    nextState = GameState.Comeback;
                } else {
                    nextState = GameState.End;
                }
            }
        } else {
            nextState = GameState.RegularTurn;
        }
        return nextState;
    }
    private void ShowComebackChanceMessage() {
        if (currentPlayer == Player.P1) {
            StartCoroutine(WriteMessage("Comeback Chance!", blueColor, 0, false));
        } else {
            StartCoroutine(WriteMessage("Comeback Chance!", redColor, 0, false));
        }
    }
    private void ShowComebackSuccessMessage() {
        if (currentPlayer == Player.P1) {
            StartCoroutine(WriteMessage("Saved!", blueColor, 0, false));
        } else {
            StartCoroutine(WriteMessage("Saved!", redColor, 0, false));
        }
    }
    private void ShowComebackFailedMessage() {
        if (currentPlayer == Player.P1) {
            //victoria de P2
            isPlaying = false;
            StartCoroutine(WriteMessage("No comeback", blueColor, 0, false));
            StartCoroutine(WriteMessage("RED VICTORY", redColor, 3, true));
        } else {
            //victoria de P1
            isPlaying = false;
            StartCoroutine(WriteMessage("No comeback", redColor, 0, false));
            StartCoroutine(WriteMessage("BLUE VICTORY", blueColor, 3, true));
        }
    }
    private void ShowJinxedMessage() {
        if (currentPlayer == Player.P1) {
            //victoria de P2
            isPlaying = false;
            StartCoroutine(WriteMessage("Jinxed!", blueColor, 0, false));
            StartCoroutine(WriteMessage("RED VICTORY", redColor, 3, true));
        } else {
            //victoria de P1
            isPlaying = false;
            StartCoroutine(WriteMessage("Jinxed!", redColor, 0, false));
            StartCoroutine(WriteMessage("BLUE VICTORY", blueColor, 3, true));
        }
    }


    void SetTurn(Player newPlayer) {
        //End the previous player turn, if there is a previous player
        if (currentPlayer != Player.Null) {
            //If sharing a controller, remove the player controller from the previous launcher
            if (gameMode == GameMode.GameOneController) {
                launchers[(int)currentPlayer].playerController = null;
            }
            launchers[(int)currentPlayer].OnTurnEnd();
        }

        //Change the current player
        currentPlayer = newPlayer;

        //If sharing a controller, add the player controller to the next launcher
        if (gameMode == GameMode.GameOneController) {
            launchers[(int)currentPlayer].playerController = playerControllers[0];
        }
        //Start the new player's turn
        launchers[(int)currentPlayer].OnTurnStart();
        launchers[(int)currentPlayer].SpawnDice();
    }
    void SelectFirstPlayer() {
        //Select a random Player to start the game
        Player startingPlayer = (Player)Random.Range(0, (int)Player.Count);
        SetTurn(startingPlayer);
        //Spawn a dice in the current player's launcher
    }
    void NextTurn() {
        //Switch current player and write down their turn-start score
        if (currentPlayer == Player.P1) {
            SetTurn(Player.P2);
        } else if (currentPlayer == Player.P2) {
            SetTurn(Player.P1);
        }
    }

    public void BackMenu() {
        StartCoroutine(CoBackMenu());
        if (isPlaying) {
            ResumeGame();
        }
    }
    IEnumerator CoBackMenu(float time = 0) {
        isBackingToMenu = true;
        yield return new WaitForSeconds(time);
        StartCoroutine(FadeImage(fade, true, 2, 0));
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    void UpdateControllerVisuals() {
        controller_share.transform.GetChild(0).GetComponent<Image>().color = blueColor;
        controller_share.transform.GetChild(1).GetComponent<Image>().color = redColor;
        controller_1.GetComponent<Image>().color = blueColor;
        controller_2.GetComponent<Image>().color = redColor;
        if (playerControllers.Count == 0) {
            controller_share.SetActive(false);
            controller_grey.SetActive(true);
            controller_1.SetActive(false); ;
            controller_2.SetActive(false); ;
            keyboard_share.SetActive(false);
            keyboard_1.SetActive(false);
            keyboard_2.SetActive(false);
        } else if (playerControllers.Count == 1) {
            controller_grey.SetActive(true);
            controller_1.SetActive(false);
            controller_2.SetActive(false);
            keyboard_1.SetActive(false);
            keyboard_2.SetActive(false);
            if (playerControllers[0].controllerType == ControllerType.Controller) {
                controller_share.SetActive(true);
            } else if (playerControllers[0].controllerType == ControllerType.Mouse) {
                keyboard_share.SetActive(true);
            }

        } else if (playerControllers.Count == 2) {
            controller_share.SetActive(false);
            controller_grey.SetActive(false);
            controller_1.SetActive(false);
            controller_2.SetActive(false);
            keyboard_share.SetActive(false);
            keyboard_1.SetActive(false);
            keyboard_2.SetActive(false);
            if (playerControllers[0].controllerType == ControllerType.Controller) {
                controller_1.SetActive(true);
            } else if (playerControllers[0].controllerType == ControllerType.Mouse) {
                keyboard_1.SetActive(true);
            }

            if (playerControllers[1].controllerType == ControllerType.Controller) {
                controller_2.SetActive(true);
            } else if (playerControllers[1].controllerType == ControllerType.Mouse) {
                keyboard_2.SetActive(true);
            }

        }
    }

    public void ResetPlayerInGame() {
        GM.Instance.gameMode = GameMode.GameTwoControllers;
        foreach (PlayerController item in playerControllers) {
            item.InitPlayer();
        }
    }

    //DICE INFO
    public List<Dice> GetDiceInsideMapBounds() {
        List<Dice> result = new List<Dice>();
        foreach (Dice d in dices) {
            if (d.IsInsideMapBounds()) {
                result.Add(d);
            }
        }
        return result;
    }
    public List<Dice> GetDiceByAreaType(Map.AreaType areaType) {
        List<Dice> result = new List<Dice>();
        foreach (Dice d in dices) {
            if (d.GetAreaTypeUnderDice() == areaType && d.IsInsideMapBounds() && d.isReleased) {
                result.Add(d);
            }
        }
        return result;
    }
    public float GetPlayer1Score() {
        return Dice.GetTotalDicesValue(GetDiceByAreaType(Map.AreaType.Player1));
    }
    public float GetPlayer2Score() {
        return Dice.GetTotalDicesValue(GetDiceByAreaType(Map.AreaType.Player2));
    }



    IEnumerator WriteMessage(string value, Color color, float delay, bool endGame = true) {
        yield return new WaitForSeconds(delay);
        textMessage.text = value;
        textMessage.color = color;
        StartCoroutine(FadeCanvasGroup(textMessage.transform.parent.GetComponent<CanvasGroup>(), true, 0.5f, 0, false));
        if (!endGame) {
            StartCoroutine(FadeCanvasGroup(textMessage.transform.parent.GetComponent<CanvasGroup>(), false, 0.5f, 1.5f, true));
        } else {
            StartCoroutine(CoBackMenu(2));
            Debug.Log("se acabo el juego");
        }
    }

    void DifferencePoints() {
        float difference = pointsEndTurn - pointsStartTurn;
        //Debug.Log("PJ"+(int)actualPlayer+ ": "+diference);
        //calcula como quieras esto y luego le pasas un int a la funcion de AudioResultTurn qu eya funciona sola

        AudioManager.Instance.AudioResultTurn((int)difference);
    }

    public void CameraShake(float shake) {
        cam.transform.DOShakePosition(1f, new Vector3(shake, shake, shake));
    }

    public void ExitGame() {
        Application.Quit();
    }
}
