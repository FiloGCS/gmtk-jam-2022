using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : Singleton<ScoreManager>
{
    public TextMeshProUGUI player1ScoreText;
    public TextMeshProUGUI player2ScoreText;
    public TextMeshProUGUI scoreDifferenceText;
    public Image scoreDifferenceBar;
    public Image borderP1;
    public Image borderP2;

    public int MAX_SCORE_DIFFERENCE = 10;

    private void Start() {
        scoreDifferenceBar.material.SetFloat("_Lambda", 0.5f);
    }
    void Update()
    {
        UpdateScore();
    }

    void UpdateScore() {
        if(GM.Instance.isPlaying){
            player1ScoreText.text = GM.Instance.GetPlayer1Score().ToString();
            player2ScoreText.text = GM.Instance.GetPlayer2Score().ToString();
            float scoreDifference = GM.Instance.GetPlayer2Score() - GM.Instance.GetPlayer1Score();
            float lambda = (scoreDifference + MAX_SCORE_DIFFERENCE) / (MAX_SCORE_DIFFERENCE * 2);
            float currentLambda = scoreDifferenceBar.material.GetFloat("_Lambda");
            lambda = Mathf.Lerp(currentLambda, lambda, Time.deltaTime * 10f);
            scoreDifferenceBar.material.SetFloat("_Lambda", lambda);
            scoreDifferenceText.text = (Mathf.Abs(scoreDifference)).ToString();
            if(GM.Instance.GetPlayer1Score()> GM.Instance.GetPlayer2Score()) {
                scoreDifferenceText.color = GM.Instance.blueColor;
            } else if(GM.Instance.GetPlayer1Score() < GM.Instance.GetPlayer2Score()) {
                scoreDifferenceText.color = GM.Instance.redColor;
            } else {
                scoreDifferenceText.color = new Color(1,1,1f,1);
            }
        }
    }

}
