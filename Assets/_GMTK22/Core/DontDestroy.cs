using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : Singleton<DontDestroy>
{
    // Start is called before the first frame update
    public bool firstGame = true;
    private void Awake() {
        base.Awake();
        DontDestroyOnLoad(gameObject);

    }

}
