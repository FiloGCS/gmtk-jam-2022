using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum ControllerType { Controller, Mouse }

public class PlayerController : MonoBehaviour
{
    public PlayerInput playerInput;
    public ControllerType controllerType;

    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if (playerInput.currentControlScheme == "Keyboard&Mouse") {
            controllerType = ControllerType.Mouse;
        } else if (playerInput.currentControlScheme == "Gamepad") {
            controllerType = ControllerType.Controller;
        }
        GM.Instance.AddPlayerController(this);
    }

    private void Start() {

        if (playerInput.playerIndex == 1) {
            if (GM.Instance.gameMode == GameMode.GameOneController) {
                GM.Instance.ResetPlayerInGame();
            } else {
                InitPlayer();
            }
        } else {
            InitPlayer();
        }
    }

    public void InitPlayer() {
        GM.GetLauncher(this).playerController = this;
    }

    //Controller Inputs
    void OnMoveLeftJoystick(InputValue value) {
        if (GM.Instance.gameMode != GameMode.Menu) {
            if (GM.GetLauncher(this) != null) {
                //Debug.Log("player: " + playerInput.playerIndex + " " + launcher.name + " " + " -- Joystick: " + value.Get());
                if (value.Get() != null)
                    GM.GetLauncher(this).UpdateLeftJoystickPosition((Vector2)value.Get());
            }
        }

    }
    void OnMoveRightJoystick(InputValue value) {
        if (GM.Instance.gameMode != GameMode.Menu) {
            if (GM.GetLauncher(this)) {
                //Debug.Log("player: " + playerInput.playerIndex + " " + launcher.name + " " + " -- Joystick: " + value.Get());
                if (value.Get() != null)
                    GM.GetLauncher(this).UpdateRightJoystickPosition((Vector2)value.Get());
            }
        }

    }
    void OnLeftShoulder(InputValue value) {
        if (GM.Instance.gameMode != GameMode.Menu) {
            if (GM.GetLauncher(this)) {
                if (value.Get() != null) {
                    GM.GetLauncher(this).LeftShoulder(value.Get<float>());
                }
            }
        }
    }
    void OnRightShoulder(InputValue value) {
        if (GM.Instance.gameMode != GameMode.Menu) {
            if (GM.GetLauncher(this)) {
                if (value.Get() != null) {
                    GM.GetLauncher(this).RightShoulder(value.Get<float>());
                }
            }
        }
    }

    //Mouse Inputs
    void OnMousePosition(InputValue value) {
        if (GM.GetLauncher(this) != null) {
            if (value.Get() != null) {
                GM.GetLauncher(this).MousePosition(value.Get<Vector2>());
            }
        }
    }
    void OnMouseScroll(InputValue value) {
        if (GM.GetLauncher(this) != null) {
            if (value.Get() != null) {
                if (value.Get<Vector2>().y > 0f) {
                    GM.GetLauncher(this).ScrollUp();
                } else {
                    GM.GetLauncher(this).ScrollDown();
                }
            }
        }
    }

    //Universal Iputs
    void OnReleaseDice(InputValue value) {
        if (GM.Instance.gameMode != GameMode.Menu) {
            if (GM.GetLauncher(this)) {
                //Debug.Log("player: " + playerInput.playerIndex + " " + launcher.name + " " + " -- gatillo: " + value.Get());
                if (value != null)
                    GM.GetLauncher(this).ReleaseDice(value);
            }
        }
    }
    void OnStart() {
        GM.Instance.PauseGame();
    }

}
