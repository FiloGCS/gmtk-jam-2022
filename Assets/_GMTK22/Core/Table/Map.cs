using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public enum AreaType { Player1, Player2, NoPlayer, Count, Null }
    [System.Serializable]
    public struct AreaColor
    {
        public AreaType area;
        public Color color;
    }

    [Header("Map Config")]
    public Texture2D mapTexture;
    public Renderer mapRenderer;
    public Transform TopLeftBound;
    public Transform BottomRightBound;
    public float diceKillingHeight = -10;

    [Header("Color Code")]
    public AreaColor[] areaColors;
    public Transform debugSampler;

    public int pixelWidth;
    public int pixelHeight;
    public float width;
    public float height;
    public Vector3 topLeftPosition;
    public Vector3 bottomRightPosition;

    private void Start() {
        topLeftPosition = TopLeftBound.position;
        bottomRightPosition = BottomRightBound.position;
        pixelWidth = mapTexture.width;
        pixelHeight = mapTexture.height;
        width = bottomRightPosition.x - topLeftPosition.x;
        height = topLeftPosition.z - bottomRightPosition.z;
        LoadMap(mapTexture);
    }

    private void Update() {
        if (debugSampler != null) {
            print(GetAreaAtPosition(debugSampler.position));
        }
    }

    public void LoadMap(Texture2D tex) {
        mapTexture = tex;
        mapRenderer.material.SetTexture("_MapTex", tex);
    }

    public Color GetColorAtPosition(Vector3 pos) {
        float u = (pos.x - topLeftPosition.x) / width;
        float v = (pos.z - bottomRightPosition.z) / height;
        return mapTexture.GetPixel((int)(pixelWidth * u), (int)(pixelHeight * v));
    }

    public AreaType GetAreaAtPosition(Vector3 pos) {
        Color positionColor = GetColorAtPosition(pos);
        Vector3 vPositionColor = (Vector4)positionColor;
        float delta = Mathf.Infinity;
        AreaType result = AreaType.Null;
        foreach(AreaColor areacolor in areaColors) {
            Vector3 vAreaColor = (Vector4)areacolor.color;
            float currentDelta = Vector3.Distance(vPositionColor, vAreaColor);
            if (delta > currentDelta) {
                result = areacolor.area;
                delta = currentDelta;
            }
        }
        return result;
    }

    public bool IsInsideMapBounds(Vector3 pos) {
        //If outside X domain
        if (pos.x < topLeftPosition.x || pos.x > bottomRightPosition.x) {
            return false;
        }
        //If outside Z domain
        if (pos.z > topLeftPosition.z || pos.z < bottomRightPosition.z) {
            return false;
        }
        return true;
    }
    public Vector3 ClampInsideBounds(Vector3 pos, bool clampY = false) {
        Vector3 result = pos;
        result.x = Mathf.Clamp(result.x, TopLeftBound.position.x, BottomRightBound.position.x);
        result.y = clampY ? transform.position.y : result.y;
        result.z = Mathf.Clamp(result.z, BottomRightBound.position.z, TopLeftBound.position.z);
        return result;
    }
}
