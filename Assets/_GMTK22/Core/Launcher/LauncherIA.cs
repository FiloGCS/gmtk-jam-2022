using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherIA : MonoBehaviour
{
    public Launcher launcher;
    public bool isActived;
    private Vector3 range;
    public GameObject rangeObject;

    public Vector2 rangeX;
    public Vector2 rangeY;
    public void InitIA() {
        StartCoroutine(IA());
        range = new Vector2(1, 0);
        rangeObject.transform.localPosition = range;
    }

    private IEnumerator IA() {
        yield return null;
        isActived = true;
        while (isActived) {
            bool isMoving = true;
            Tween myTween = null;
            float myTime = Random.Range(0.8f, 1.5f);
            Debug.Log("myTime: " + myTime);
            while (isMoving) {
                if (myTween == null) {
                    myTween = rangeObject.transform.DOLocalMove(new Vector3(Random.Range(rangeX.x * 100, rangeX.y * 100) / 100f, Random.Range(rangeY.x * 100, rangeY.y * 100) / 100f, 0), myTime).SetEase(Ease.InSine)
                        .OnComplete(() => {
                            if (Random.Range(0, 2) == 0) {
                                isMoving = false;
                                launcher.ReleaseDice();
                                Invoke("LoadDice", 0.2f);
                            }
                            myTween = null;
                        });
                }
                range = new Vector2(rangeObject.transform.localPosition.x, rangeObject.transform.localPosition.y);
                launcher.UpdateLeftJoystickPosition(range);
                yield return new WaitForEndOfFrame();
            }
            float timeWait = Random.Range(3f, 10f) / 10f;
            Debug.Log("timeWait: " + timeWait);
            yield return new WaitForSeconds(Random.Range(3f, 10f) / 10f);
        }

    }

    private void LoadDice() {
        launcher.LoadDice();
    }
}
