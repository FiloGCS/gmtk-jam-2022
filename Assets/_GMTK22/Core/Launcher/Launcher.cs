using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Launcher : MonoBehaviour
{

    public float TESTING = 1.0f;

    //REFERENCES
    [Header("REFERENCES")]
    public GameObject diceGameObject;
    public Transform myReticle;
    public Transform minimumPowerPosition;
    public Transform maximumPowerPosition;
    public PlayerController playerController;
    public LineRenderer lineRenderer;
    public AnimationTutorial myTuto;

    //PARAMETERS
    [Header("PARAMETERS")]
    public Vector2 reticleMinPosition;
    public Vector2 reticleMaxPosition;
    public float reticleMovementSpeed = 20f;
    public AnimationCurve reticleSensibilityCurve;
    public AnimationCurve parabolaDurationCurve;
    public float minParabolaDurationWithBias = 1.0f;

    //VARIABLES
    [Header("VARIABLES")]
    //Dice
    public GameObject loadedDice = null;
    //Input
    private Vector2 leftJoystick = Vector2.zero;
    private Vector2 rightJoystick = Vector2.zero;
    private Vector2 mousePosition = Vector2.zero;
    private float virtualRightJoystick = 0;
    private Vector2 reticlePosition = new Vector2(0f, 0f);
    //Aiming
    private Vector3 targetPosition;
    public Vector3 launchVelocity = Vector3.zero;
    public Vector3 launchAngularVelocity = Vector3.zero;
    public float parabolaDuration = 1.0f;
    //Turns
    public bool isMyTurn;

    //METHODS
    void Start() {
        lineRenderer = GetComponentInChildren<LineRenderer>();
    }
    void Update() {
        UpdateTutorial();
        //If we have a dice loaded
        if (loadedDice != null) {

            //Update my reticle
            UpdateReticlePosition();
            myReticle.transform.position = new Vector3(reticlePosition.x, 0, reticlePosition.y);
            myReticle.gameObject.SetActive(true);

            //Look at our launch direction
            launchVelocity = CalculateRequiredVelocity();
            this.transform.rotation = Quaternion.LookRotation(launchVelocity.normalized);

            //Update the dice position depending on the required launch velocity
            Vector3 dicePositionOffset = Vector3.Lerp(minimumPowerPosition.position, maximumPowerPosition.position, (rightJoystick.y+1)/2.0f);
            loadedDice.transform.position = dicePositionOffset;
        } else {

            myReticle.gameObject.SetActive(false);
        }
        UpdateTrajectoryPreview();
    }

    //DICE MANAGING
    public void SpawnDice() {
        loadedDice = Instantiate<GameObject>(diceGameObject, minimumPowerPosition.position, Random.rotation);
        //Disable Rigidbody
        loadedDice.GetComponent<Rigidbody>().useGravity = false;
        loadedDice.GetComponent<Rigidbody>().isKinematic = true;
    }
    public void LoadDice() {
        SpawnDice();
    }
    public void ReleaseDice() {
        //Enable Rigidbody
        loadedDice.GetComponent<Rigidbody>().useGravity = true;
        loadedDice.GetComponent<Rigidbody>().isKinematic = false;

        //Apply required velocity
        loadedDice.GetComponent<Rigidbody>().velocity = launchVelocity;
        //Apply angular velocity
        //loadedDice.GetComponent<Rigidbody>().angularVelocity = launchAngularVelocity;
        loadedDice.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * 10f;

        loadedDice.GetComponent<Dice>().Throw();
        loadedDice = null;
    }
    public void ReleaseDice(InputValue value = null) {
        if (loadedDice != null) {
            ReleaseDice();
        }
    }

    //AIMING
    public void UpdateReticlePosition() {
        if(playerController.controllerType == ControllerType.Mouse) { //Mouse
            UpdateReticlepositionMouse();
        } else { //Controller
            UpdateReticlePositionController();
        }
    }
    private void UpdateReticlepositionMouse() {
        Ray r = Camera.main.ScreenPointToRay(mousePosition);
        Plane p = new Plane(Vector3.up, GM.map.transform.position.y);
        float d;
        p.Raycast(r, out d);
        Vector3 pos = r.origin + r.direction * d;
        pos = GM.map.ClampInsideBounds(pos,true);
        reticlePosition = new(pos.x, pos.z);
    }
    private void UpdateReticlePositionController() {
        //Convert from joystick position to reticle velocity using the sensibility curve
        Vector2 reticleV = new Vector2(reticleSensibilityCurve.Evaluate(Mathf.Abs(leftJoystick.x)), reticleSensibilityCurve.Evaluate(Mathf.Abs(leftJoystick.y)));
        //Update Reticle Position
        reticlePosition.x += leftJoystick.x * reticleV.x * Time.deltaTime * reticleMovementSpeed;
        reticlePosition.y += leftJoystick.y * reticleV.y * Time.deltaTime * reticleMovementSpeed;
        //Limit to the min/max aim ranges
        reticlePosition.x = reticlePosition.x > reticleMaxPosition.x ? reticleMaxPosition.x : reticlePosition.x;
        reticlePosition.x = reticlePosition.x < reticleMinPosition.x ? reticleMinPosition.x : reticlePosition.x;
        reticlePosition.y = reticlePosition.y > reticleMaxPosition.y ? reticleMaxPosition.y : reticlePosition.y;
        reticlePosition.y = reticlePosition.y < reticleMinPosition.y ? reticleMinPosition.y : reticlePosition.y;
    }

    public Vector3 CalculateRequiredVelocity() {
        targetPosition = new Vector3(reticlePosition.x, 0f, reticlePosition.y);
        float distance = Vector3.Distance(loadedDice.transform.position, targetPosition);
        //parabolaDuration evaluates the curve which tells the range of tallness in the parabola depending on the right joystick value

        if (playerController.controllerType == ControllerType.Mouse) { //Mouse
            parabolaDuration = parabolaDurationCurve.Evaluate(virtualRightJoystick);
        } else {
            parabolaDuration = parabolaDurationCurve.Evaluate(rightJoystick.y);
        }
        parabolaDuration *= distance;
        float clampedParabolaDuration = Mathf.Max(1, parabolaDuration);
        //Bias pulls up parabolas in very close shots while not having as great an effect on the long shots
        //float bias = Mathf.Clamp01(-rightJoystick.y)*0.3f;
        float bias = Mathf.Clamp01(-rightJoystick.y);
        parabolaDuration = Mathf.Lerp(parabolaDuration, clampedParabolaDuration, bias);
        return Mathgm.GetParabolaInitialVelocity(loadedDice.transform.position, targetPosition, Physics.gravity, parabolaDuration);
    }

    //TRAJECTORY CALCULATIONS
    public static Vector3 GetTrajectory(Vector3 p0, Vector3 v0, float t) {
        Vector3 p = Vector3.zero;
        p.x = v0.x * t + p0.x;
        p.z = v0.z * t + p0.z;
        p.y = (Physics.gravity.y * 0.5f * t * t) + v0.y * t + p0.y;
        return p;
    }
    public Vector3[] GetTrajectoryPoints(Vector3 p0, Vector3 v0, float duration, float samples) {
        float mapHeight = GM.map.transform.position.y;
        List<Vector3> result = new List<Vector3>();
        //print(mapHeight);
        float t = 0;
        Vector3 pA = p0;
        result.Add(pA);
        Vector3 pB;
        for (int i = 0; i < samples; i++) {
            //print(t);
            t = (float)i / (float)samples * duration;
            pB = GetTrajectory(p0, v0, t);
            Debug.DrawLine(pA, pB, Color.white);
            pA = pB;
            result.Add(pA);
            if (pA.y < mapHeight) {
                break;
            }
        }
        return result.ToArray();
    }
    void ShowTrajectoryPreview(Vector3 p0, Vector3 v0, float duration) {
        Vector3[] positions = GetTrajectoryPoints(p0, v0, duration, 50);
        lineRenderer.positionCount = positions.Length;
        lineRenderer.SetPositions(positions);
        lineRenderer.enabled = true;
    }
    void ClearTrajectoryPreview() {
        lineRenderer.enabled = false;
    }
    void UpdateTrajectoryPreview() {
        if (loadedDice != null) {
            ShowTrajectoryPreview(loadedDice.transform.position, launchVelocity, parabolaDuration);
        } else {
            ClearTrajectoryPreview();
        }
    }

    //INPUT
    //Controller
    public void UpdateLeftJoystickPosition(Vector2 value) {
        leftJoystick = value;
    }
    public void UpdateRightJoystickPosition(Vector2 value) {
        rightJoystick = value;
    }
    public void LeftShoulder(float value) {
        //leftShoulderPressed = value != 0.0f;
    }
    public void RightShoulder(float value) {
        //rightShoulderPressed = value != 0.0f;
    }
    //Mouse
    public void ScrollUp() {
        virtualRightJoystick = Mathf.Clamp(virtualRightJoystick + 0.15f, -1, 1);
    }
    public void ScrollDown() {
        virtualRightJoystick = Mathf.Clamp(virtualRightJoystick - 0.15f, -1, 1);
    }
    public void MousePosition(Vector2 value) {
        if(playerController.controllerType == ControllerType.Mouse) {
            mousePosition = value;
        }
    }

    //TUTORIAL
    public void UpdateTutorial() {
        //if it's my turn and the player has barely moved the joystick
        if (loadedDice != null) {
            myTuto.Trigger();
            myTuto.Joystick();
            myTuto.Button();
        } else {
            myTuto.Hide();
        }
    }

    //TURN MANAGING
    public void OnTurnStart() {
        isMyTurn = true;
    }
    public void OnTurnEnd() {
        isMyTurn = false;
    }
}
