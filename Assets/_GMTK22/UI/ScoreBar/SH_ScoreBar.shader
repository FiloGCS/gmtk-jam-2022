// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_ScoreBar"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_Lambda("Lambda", Range( 0 , 1)) = 0.5
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TextureSample1("Texture Sample 0", 2D) = "white" {}
		_BlueSideSpeed("Blue Side Speed", Vector) = (-0.1,0.01,0,0)
		_RedSideSpeed("Red Side Speed", Vector) = (0.1,-0.01,0,0)
		_Tiling("Tiling", Vector) = (1,1,0,0)
		_EmissiveStrength("Emissive Strength", Float) = 0

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float _EmissiveStrength;
			uniform sampler2D _TextureSample1;
			uniform float2 _BlueSideSpeed;
			uniform float2 _RedSideSpeed;
			uniform float _Lambda;
			uniform float2 _Tiling;
			uniform sampler2D _TextureSample0;
			struct Gradient
			{
				int type;
				int colorsLength;
				int alphasLength;
				float4 colors[8];
				float2 alphas[8];
			};
			
			Gradient NewGradient(int type, int colorsLength, int alphasLength, 
			float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
			float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
			{
				Gradient g;
				g.type = type;
				g.colorsLength = colorsLength;
				g.alphasLength = alphasLength;
				g.colors[ 0 ] = colors0;
				g.colors[ 1 ] = colors1;
				g.colors[ 2 ] = colors2;
				g.colors[ 3 ] = colors3;
				g.colors[ 4 ] = colors4;
				g.colors[ 5 ] = colors5;
				g.colors[ 6 ] = colors6;
				g.colors[ 7 ] = colors7;
				g.alphas[ 0 ] = alphas0;
				g.alphas[ 1 ] = alphas1;
				g.alphas[ 2 ] = alphas2;
				g.alphas[ 3 ] = alphas3;
				g.alphas[ 4 ] = alphas4;
				g.alphas[ 5 ] = alphas5;
				g.alphas[ 6 ] = alphas6;
				g.alphas[ 7 ] = alphas7;
				return g;
			}
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				Gradient gradient31 = NewGradient( 0, 5, 2, float4( 0, 0.1486878, 0.6603774, 0 ), float4( 0, 0.272781, 0.7058824, 0.1117723 ), float4( 0, 0.6434555, 0.8301887, 0.5176471 ), float4( 0.427451, 0.9634021, 1, 0.8205844 ), float4( 0.6367924, 0.9776104, 1, 1 ), 0, 0, 0, float2( 1, 0.1000076 ), float2( 1, 0.8 ), 0, 0, 0, 0, 0, 0 );
				float2 texCoord15 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_17_0 = ceil( ( texCoord15.x - ( 1.0 - saturate( _Lambda ) ) ) );
				float2 lerpResult27 = lerp( _BlueSideSpeed , _RedSideSpeed , temp_output_17_0);
				float2 texCoord36 = IN.texcoord.xy * _Tiling + float2( 0,0 );
				float2 panner37 = ( 1.0 * _Time.y * ( lerpResult27 * float2( 2.3,2.3 ) ) + texCoord36);
				float2 texCoord28 = IN.texcoord.xy * _Tiling + float2( 0,0 );
				float2 panner24 = ( 1.0 * _Time.y * lerpResult27 + texCoord28);
				float temp_output_40_0 = (( tex2D( _TextureSample1, panner37 ) * tex2D( _TextureSample0, panner24 ) )).g;
				Gradient gradient32 = NewGradient( 0, 4, 2, float4( 0.8396226, 0.04978716, 0, 0 ), float4( 1, 0.4048133, 0, 0.3117723 ), float4( 1, 0.6714561, 0, 0.5117723 ), float4( 1, 0.8708706, 0.4764151, 0.8941177 ), 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float4 lerpResult18 = lerp( SampleGradient( gradient31, temp_output_40_0 ) , SampleGradient( gradient32, temp_output_40_0 ) , temp_output_17_0);
				
				half4 color = ( _EmissiveStrength * saturate( lerpResult18 ) );
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18935
252;73;1047;584;490.6689;896.2116;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;13;-2277.374,134.3213;Inherit;False;Property;_Lambda;Lambda;0;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;21;-1912.273,198.9452;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-2049.229,22.28265;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;22;-1741.272,225.9452;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;16;-1781.591,67.81606;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;25;-1501.9,-627.3203;Inherit;False;Property;_BlueSideSpeed;Blue Side Speed;3;0;Create;True;0;0;0;False;0;False;-0.1,0.01;-0.1,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;26;-1487.9,-496.3203;Inherit;False;Property;_RedSideSpeed;Red Side Speed;4;0;Create;True;0;0;0;False;0;False;0.1,-0.01;0.1,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.CeilOpNode;17;-1616.374,63.92136;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;27;-1149.9,-512.3203;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;41;-1443.365,-1013.894;Inherit;False;Property;_Tiling;Tiling;5;0;Create;True;0;0;0;False;0;False;1,1;21.69,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;36;-1015.923,-1055.882;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-949.1501,-493.7872;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;2.3,2.3;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1177.446,-749.5106;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;37;-794.3776,-935.692;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;24;-955.9004,-629.3203;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;23;-568.7898,-526.0318;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;0;False;0;False;-1;98dfc8802edf0fa49ba8fdc9bbad5866;a39d3ae4b5bcff840ad648ff4e0fd591;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;34;-621.2663,-773.6238;Inherit;True;Property;_TextureSample1;Texture Sample 0;2;0;Create;True;0;0;0;False;0;False;-1;98dfc8802edf0fa49ba8fdc9bbad5866;a39d3ae4b5bcff840ad648ff4e0fd591;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-239.2166,-578.6984;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;40;-10.21655,-534.6984;Inherit;False;False;True;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientNode;31;42.34961,-770.1642;Inherit;False;0;5;2;0,0.1486878,0.6603774,0;0,0.272781,0.7058824,0.1117723;0,0.6434555,0.8301887,0.5176471;0.427451,0.9634021,1,0.8205844;0.6367924,0.9776104,1,1;1,0.1000076;1,0.8;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientNode;32;-120.1504,-651.8644;Inherit;False;0;4;2;0.8396226,0.04978716,0,0;1,0.4048133,0,0.3117723;1,0.6714561,0,0.5117723;1,0.8708706,0.4764151,0.8941177;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientSampleNode;30;278.3596,-737.8654;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;33;278.2052,-534.5706;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;18;717.0568,-377.3828;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;44;960,-368;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;43;896,-464;Inherit;False;Property;_EmissiveStrength;Emissive Strength;6;0;Create;True;0;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;-832,-178.5;Inherit;False;Constant;_Player2Color;Player 2 Color;0;0;Create;True;0;0;0;False;0;False;1,0.2260174,0,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;10;-826,-382.5;Inherit;False;Constant;_Player1Color;Player 1 Color;0;0;Create;True;0;0;0;False;0;False;0,0.5453134,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;1120,-432;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;19;1312,-432;Float;False;True;-1;2;ASEMaterialInspector;0;6;SH_ScoreBar;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;21;0;13;0
WireConnection;22;0;21;0
WireConnection;16;0;15;1
WireConnection;16;1;22;0
WireConnection;17;0;16;0
WireConnection;27;0;25;0
WireConnection;27;1;26;0
WireConnection;27;2;17;0
WireConnection;36;0;41;0
WireConnection;35;0;27;0
WireConnection;28;0;41;0
WireConnection;37;0;36;0
WireConnection;37;2;35;0
WireConnection;24;0;28;0
WireConnection;24;2;27;0
WireConnection;23;1;24;0
WireConnection;34;1;37;0
WireConnection;38;0;34;0
WireConnection;38;1;23;0
WireConnection;40;0;38;0
WireConnection;30;0;31;0
WireConnection;30;1;40;0
WireConnection;33;0;32;0
WireConnection;33;1;40;0
WireConnection;18;0;30;0
WireConnection;18;1;33;0
WireConnection;18;2;17;0
WireConnection;44;0;18;0
WireConnection;42;0;43;0
WireConnection;42;1;44;0
WireConnection;19;0;42;0
ASEEND*/
//CHKSM=A8378F83BBF65D81296C8BCAA03B5DD54DD3ABB1