// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Panel"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_BorderPattern("Border Pattern", 2D) = "white" {}
		_Lambda("Lambda", Range( 0 , 1)) = 0.5
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TextureSample1("Texture Sample 0", 2D) = "white" {}
		_BlueSideSpeed("Blue Side Speed", Vector) = (-0.1,0.01,0,0)
		_RedSideSpeed("Red Side Speed", Vector) = (0.1,-0.01,0,0)
		_MainTex("MainTex", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord2 : TEXCOORD2;
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _TextureSample1;
			uniform float2 _BlueSideSpeed;
			uniform float2 _RedSideSpeed;
			uniform float _Lambda;
			uniform sampler2D _TextureSample0;
			uniform sampler2D _BorderPattern;
			uniform float4 _MainTex_ST;
			struct Gradient
			{
				int type;
				int colorsLength;
				int alphasLength;
				float4 colors[8];
				float2 alphas[8];
			};
			
			Gradient NewGradient(int type, int colorsLength, int alphasLength, 
			float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
			float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
			{
				Gradient g;
				g.type = type;
				g.colorsLength = colorsLength;
				g.alphasLength = alphasLength;
				g.colors[ 0 ] = colors0;
				g.colors[ 1 ] = colors1;
				g.colors[ 2 ] = colors2;
				g.colors[ 3 ] = colors3;
				g.colors[ 4 ] = colors4;
				g.colors[ 5 ] = colors5;
				g.colors[ 6 ] = colors6;
				g.colors[ 7 ] = colors7;
				g.alphas[ 0 ] = alphas0;
				g.alphas[ 1 ] = alphas1;
				g.alphas[ 2 ] = alphas2;
				g.alphas[ 3 ] = alphas3;
				g.alphas[ 4 ] = alphas4;
				g.alphas[ 5 ] = alphas5;
				g.alphas[ 6 ] = alphas6;
				g.alphas[ 7 ] = alphas7;
				return g;
			}
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				float4 ase_clipPos = UnityObjectToClipPos(IN.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				OUT.ase_texcoord2 = screenPos;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				Gradient gradient40 = NewGradient( 0, 3, 2, float4( 0, 0.272781, 0.7058824, 0 ), float4( 0, 0.6434555, 0.8301887, 0.723537 ), float4( 0.427451, 0.9634021, 1, 1 ), 0, 0, 0, 0, 0, float2( 1, 0.1000076 ), float2( 1, 0.8 ), 0, 0, 0, 0, 0, 0 );
				float2 texCoord45 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_48_0 = ceil( ( texCoord45.x - ( 1.0 - saturate( _Lambda ) ) ) );
				float2 lerpResult49 = lerp( _BlueSideSpeed , _RedSideSpeed , temp_output_48_0);
				float4 screenPos = IN.ase_texcoord2;
				float2 appendResult67 = (float2(screenPos.x , screenPos.y));
				float2 appendResult68 = (float2(_ScreenParams.x , _ScreenParams.y));
				float2 temp_output_70_0 = ( ( appendResult67 * appendResult68 ) / float2( 253.7,164.9 ) );
				float2 panner54 = ( 1.0 * _Time.y * ( lerpResult49 * float2( 1.32,1.32 ) ) + temp_output_70_0);
				float2 panner53 = ( 1.0 * _Time.y * lerpResult49 + temp_output_70_0);
				float temp_output_39_0 = (( tex2D( _TextureSample1, panner54 ) * tex2D( _TextureSample0, panner53 ) )).g;
				Gradient gradient58 = NewGradient( 0, 3, 2, float4( 1, 0.2584994, 0, 0 ), float4( 1, 0.4048133, 0, 0.5000076 ), float4( 1, 0.6714561, 0, 0.9676509 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float4 lerpResult43 = lerp( SampleGradient( gradient40, temp_output_39_0 ) , SampleGradient( gradient58, temp_output_39_0 ) , temp_output_48_0);
				float2 appendResult16 = (float2(screenPos.x , screenPos.y));
				float2 appendResult27 = (float2(_ScreenParams.x , _ScreenParams.y));
				float2 panner31 = ( 1.0 * _Time.y * float2( -0.05,0.032 ) + ( ( appendResult16 * appendResult27 ) / float2( 100,100 ) ));
				float4 lerpResult19 = lerp( IN.color , ( IN.color + float4( 0.3113208,0.3113208,0.3113208,0 ) ) , tex2D( _BorderPattern, panner31 ).r);
				float2 uv_MainTex = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode4 = tex2D( _MainTex, uv_MainTex );
				float4 lerpResult6 = lerp( lerpResult43 , lerpResult19 , tex2DNode4.r);
				float4 temp_output_12_0 = ( lerpResult6 * tex2DNode4.a );
				
				half4 color = temp_output_12_0;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18935
274;73;1136;575;3745.456;1618.171;2.979988;True;False
Node;AmplifyShaderEditor.RangedFloatNode;38;-5245.629,-622.0533;Inherit;False;Property;_Lambda;Lambda;1;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;44;-4880.528,-557.4294;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;46;-4709.527,-530.4295;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;45;-5017.484,-734.092;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;64;-5321.396,-2232.605;Inherit;False;678.8;473.1939;ScreenSpace UV;5;69;68;67;66;65;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;47;-4749.846,-688.5586;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenParams;65;-5212.964,-1966.412;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;66;-5271.396,-2182.605;Float;True;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;62;-4456.155,-1252.695;Inherit;False;Property;_RedSideSpeed;Red Side Speed;9;0;Create;True;0;0;0;False;0;False;0.1,-0.01;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.CeilOpNode;48;-4584.629,-692.4532;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;67;-5012.596,-2154.706;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;68;-5012.596,-1946.706;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;61;-4470.155,-1383.695;Inherit;False;Property;_BlueSideSpeed;Blue Side Speed;7;0;Create;True;0;0;0;False;0;False;-0.1,0.01;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.LerpOp;49;-4118.155,-1268.695;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;-4804.596,-2074.706;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;5,5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;34;-2798.8,-566.8998;Inherit;False;678.8;473.1939;ScreenSpace UV;5;26;15;16;27;25;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;15;-2748.8,-516.8998;Float;True;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;70;-4570.596,-1665.706;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;253.7,164.9;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-3917.406,-1250.162;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;1.32,1.32;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenParams;26;-2690.368,-300.7059;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;53;-3924.156,-1385.695;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;16;-2490,-489;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-2490,-281;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;54;-3762.633,-1692.067;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-2282,-409;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;5,5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;56;-3589.522,-1529.999;Inherit;True;Property;_TextureSample1;Texture Sample 0;5;0;Create;True;0;0;0;False;0;False;-1;98dfc8802edf0fa49ba8fdc9bbad5866;98dfc8802edf0fa49ba8fdc9bbad5866;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;55;-3537.045,-1282.406;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;-1;98dfc8802edf0fa49ba8fdc9bbad5866;98dfc8802edf0fa49ba8fdc9bbad5866;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;-3207.472,-1335.073;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;29;-2048,0;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;100,100;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;31;-1872,0;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.05,0.032;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;39;-2978.472,-1291.073;Inherit;False;False;True;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;5;-1409.108,178.189;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientNode;58;-3088.406,-1408.24;Inherit;False;0;3;2;1,0.2584994,0,0;1,0.4048133,0,0.5000076;1,0.6714561,0,0.9676509;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientNode;40;-2925.906,-1526.539;Inherit;False;0;3;2;0,0.272781,0.7058824,0;0,0.6434555,0.8301887,0.723537;0.427451,0.9634021,1,1;1,0.1000076;1,0.8;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientSampleNode;41;-2690.05,-1290.945;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;37;-1013.351,49.7319;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.3113208,0.3113208,0.3113208,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;13;-1648,0;Inherit;True;Property;_BorderPattern;Border Pattern;0;0;Create;True;0;0;0;False;0;False;-1;a1bc80d4e9339f1498a0fae13dde6fe1;0240f92c03016ef42bd6f6a830db9fbd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;42;-2689.896,-1494.24;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-847.0022,164.9512;Inherit;True;Property;_MainTex;MainTex;10;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;43;-2251.198,-1133.757;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;19;-873.0791,-70.72662;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;11;-282.7502,392.7105;Inherit;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;6;-304,-304;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;10;277,133;Inherit;True;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;498,-85;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;60;-3794.255,-1138.875;Inherit;False;Constant;_Player1Color;Player 1 Color;0;0;Create;True;0;0;0;False;0;False;0,0.5453134,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;20;-1344,-160;Inherit;False;Property;_BorderColor1;Border Color 1;8;0;Create;True;0;0;0;False;0;False;1,0.7674963,0.3443396,1;1,0.7674963,0.3443396,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;59;-3800.255,-934.8746;Inherit;False;Constant;_Player2Color;Player 2 Color;0;0;Create;True;0;0;0;False;0;False;1,0.2260174,0,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;9;20.90433,46.01823;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;24;-1602.036,-833.3863;Inherit;False;Property;_BGColor1;BG Color 1;6;0;Create;True;0;0;0;False;0;False;0.08490568,0.0805002,0.0805002,1;0.08490568,0.0805002,0.0805002,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;71;-627.8733,-255.8521;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;21;-1344,-352;Inherit;False;Property;_BorderColor0;Border Color 0;2;0;Create;True;0;0;0;False;0;False;0.8867924,0.5637382,0.2133321,1;0.8867924,0.5637382,0.2133321,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;650.2648,-193.7231;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;23;-1248,-608;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;7;-1517.469,-1045.212;Inherit;False;Property;_BGColor0;BG Color 0;4;0;Create;True;0;0;0;False;0;False;0.0471698,0.0471698,0.0471698,1;0.0471698,0.0471698,0.0471698,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;33;816,-80;Float;False;True;-1;2;ASEMaterialInspector;0;6;SH_Panel;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;44;0;38;0
WireConnection;46;0;44;0
WireConnection;47;0;45;1
WireConnection;47;1;46;0
WireConnection;48;0;47;0
WireConnection;67;0;66;1
WireConnection;67;1;66;2
WireConnection;68;0;65;1
WireConnection;68;1;65;2
WireConnection;49;0;61;0
WireConnection;49;1;62;0
WireConnection;49;2;48;0
WireConnection;69;0;67;0
WireConnection;69;1;68;0
WireConnection;70;0;69;0
WireConnection;51;0;49;0
WireConnection;53;0;70;0
WireConnection;53;2;49;0
WireConnection;16;0;15;1
WireConnection;16;1;15;2
WireConnection;27;0;26;1
WireConnection;27;1;26;2
WireConnection;54;0;70;0
WireConnection;54;2;51;0
WireConnection;25;0;16;0
WireConnection;25;1;27;0
WireConnection;56;1;54;0
WireConnection;55;1;53;0
WireConnection;57;0;56;0
WireConnection;57;1;55;0
WireConnection;29;0;25;0
WireConnection;31;0;29;0
WireConnection;39;0;57;0
WireConnection;41;0;58;0
WireConnection;41;1;39;0
WireConnection;37;0;5;0
WireConnection;13;1;31;0
WireConnection;42;0;40;0
WireConnection;42;1;39;0
WireConnection;43;0;42;0
WireConnection;43;1;41;0
WireConnection;43;2;48;0
WireConnection;19;0;5;0
WireConnection;19;1;37;0
WireConnection;19;2;13;1
WireConnection;11;0;4;4
WireConnection;6;0;43;0
WireConnection;6;1;19;0
WireConnection;6;2;4;1
WireConnection;10;0;9;0
WireConnection;10;3;4;4
WireConnection;12;0;6;0
WireConnection;12;1;11;0
WireConnection;9;0;6;0
WireConnection;35;0;5;0
WireConnection;35;1;12;0
WireConnection;23;0;7;0
WireConnection;23;1;24;0
WireConnection;33;0;12;0
ASEEND*/
//CHKSM=70B4C29A04A2D2E1909AE16F460065B22D19C76E