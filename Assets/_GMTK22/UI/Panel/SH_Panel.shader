// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Panel"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_BorderPattern("Border Pattern", 2D) = "white" {}
		_BGColor0("BG Color 0", Color) = (0.0471698,0.0471698,0.0471698,1)
		_BGColor1("BG Color 1", Color) = (0.08490568,0.0805002,0.0805002,1)
		_MainTex("MainTex", 2D) = "white" {}
		_BackgroundPattern("Background Pattern", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord2 : TEXCOORD2;
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float4 _BGColor0;
			uniform float4 _BGColor1;
			uniform sampler2D _BackgroundPattern;
			uniform sampler2D _BorderPattern;
			uniform float4 _MainTex_ST;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				float4 ase_clipPos = UnityObjectToClipPos(IN.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				OUT.ase_texcoord2 = screenPos;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float4 screenPos = IN.ase_texcoord2;
				float2 appendResult16 = (float2(screenPos.x , screenPos.y));
				float2 appendResult27 = (float2(_ScreenParams.x , _ScreenParams.y));
				float2 temp_output_25_0 = ( appendResult16 * appendResult27 );
				float4 lerpResult23 = lerp( _BGColor0 , _BGColor1 , tex2D( _BackgroundPattern, ( temp_output_25_0 / float2( 500,500 ) ) ).r);
				float2 panner31 = ( 1.0 * _Time.y * float2( -0.05,0.032 ) + ( temp_output_25_0 / float2( 100,100 ) ));
				float4 lerpResult19 = lerp( IN.color , ( IN.color + float4( 0.3113208,0.3113208,0.3113208,0 ) ) , tex2D( _BorderPattern, panner31 ).r);
				float2 uv_MainTex = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode4 = tex2D( _MainTex, uv_MainTex );
				float4 lerpResult6 = lerp( lerpResult23 , lerpResult19 , tex2DNode4.r);
				
				half4 color = ( IN.color * ( lerpResult6 * tex2DNode4.a ) );
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18935
274;73;1136;575;3223.717;1591.956;2.701103;True;False
Node;AmplifyShaderEditor.CommentaryNode;34;-2798.8,-566.8998;Inherit;False;678.8;473.1939;ScreenSpace UV;5;26;15;16;27;25;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenParams;26;-2690.368,-300.7059;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;15;-2748.8,-516.8998;Float;True;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;16;-2490,-489;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-2490,-281;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-2282,-409;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;5,5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;29;-2048,0;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;100,100;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;31;-1872,0;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.05,0.032;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;32;-1888,-560;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;500,500;False;1;FLOAT2;0
Node;AmplifyShaderEditor.VertexColorNode;5;-1031.902,-749.9263;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;-1602.036,-833.3863;Inherit;False;Property;_BGColor1;BG Color 1;3;0;Create;True;0;0;0;False;0;False;0.08490568,0.0805002,0.0805002,1;0.08490568,0.0805002,0.0805002,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;13;-1648,0;Inherit;True;Property;_BorderPattern;Border Pattern;0;0;Create;True;0;0;0;False;0;False;-1;a1bc80d4e9339f1498a0fae13dde6fe1;0240f92c03016ef42bd6f6a830db9fbd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;7;-1517.469,-1045.212;Inherit;False;Property;_BGColor0;BG Color 0;2;0;Create;True;0;0;0;False;0;False;0.0471698,0.0471698,0.0471698,1;0.0471698,0.0471698,0.0471698,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;37;-837.8837,-311.9207;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.3113208,0.3113208,0.3113208,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;30;-1696,-592;Inherit;True;Property;_BackgroundPattern;Background Pattern;6;0;Create;True;0;0;0;False;0;False;-1;a1bc80d4e9339f1498a0fae13dde6fe1;a1bc80d4e9339f1498a0fae13dde6fe1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;23;-1248,-608;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;19;-1040,-80;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;4;-847.0022,164.9512;Inherit;True;Property;_MainTex;MainTex;5;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;11;-282.7502,392.7105;Inherit;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;6;-304,-304;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;498,-85;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;21;-1344,-352;Inherit;False;Property;_BorderColor0;Border Color 0;1;0;Create;True;0;0;0;False;0;False;0.8867924,0.5637382,0.2133321,1;0.8867924,0.5637382,0.2133321,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;9;20.90433,46.01823;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;10;277,133;Inherit;True;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;36;-846.6625,-753.1598;Inherit;True;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;650.2648,-193.7231;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;20;-1344,-160;Inherit;False;Property;_BorderColor1;Border Color 1;4;0;Create;True;0;0;0;False;0;False;1,0.7674963,0.3443396,1;1,0.7674963,0.3443396,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;33;816,-80;Float;False;True;-1;2;ASEMaterialInspector;0;6;SH_Panel;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;16;0;15;1
WireConnection;16;1;15;2
WireConnection;27;0;26;1
WireConnection;27;1;26;2
WireConnection;25;0;16;0
WireConnection;25;1;27;0
WireConnection;29;0;25;0
WireConnection;31;0;29;0
WireConnection;32;0;25;0
WireConnection;13;1;31;0
WireConnection;37;0;5;0
WireConnection;30;1;32;0
WireConnection;23;0;7;0
WireConnection;23;1;24;0
WireConnection;23;2;30;1
WireConnection;19;0;5;0
WireConnection;19;1;37;0
WireConnection;19;2;13;1
WireConnection;11;0;4;4
WireConnection;6;0;23;0
WireConnection;6;1;19;0
WireConnection;6;2;4;1
WireConnection;12;0;6;0
WireConnection;12;1;11;0
WireConnection;9;0;6;0
WireConnection;10;0;9;0
WireConnection;10;3;4;4
WireConnection;36;0;5;0
WireConnection;35;0;5;0
WireConnection;35;1;12;0
WireConnection;33;0;35;0
ASEEND*/
//CHKSM=CCEF8846400261279F6EC36A33E0064AD40F369B