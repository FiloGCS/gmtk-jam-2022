using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AnimationTutorial : MonoBehaviour
{
    public Image mando;
    public Image joy_izq_color;
    public Image button;
    public Image trigger;
    public Player actualPlayer;
    private void Start() {

        Hide();
    }

    public void Hide() {
        mando.enabled = false;
        joy_izq_color.enabled = false;
        button.enabled = false;
        trigger.enabled = false;
    }

    public void Joystick() {
        mando.enabled = true;
        joy_izq_color.enabled = true;
        //button.enabled = false;
        //trigger.enabled = false;
    }

    public void Trigger() {
        mando.enabled = true;
        trigger.enabled = true;
    }

    public void Button() {
        mando.enabled = true;
        //joy_izq_color.enabled = false;
        button.enabled = true;
        //trigger.enabled = false;
    }

    public void Aim() {
        mando.enabled = true;
        //joy_izq_color.enabled = false;
        //button.enabled = false;
        trigger.enabled = true;
    }


}
