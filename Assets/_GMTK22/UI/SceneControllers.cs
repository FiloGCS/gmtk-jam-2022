using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneControllers : MonoBehaviour {
    public Image image;
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(FadeImage());
    }

    IEnumerator FadeImage() {
        image.color = new Color(image.color.r,image.color.g,image.color.b,1);
        while(image.color.a > 0) {
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a - Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(4f);

        while(image.color.a< 1) {
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a + Time.deltaTime);
            yield return null;
        }

        SceneManager.LoadScene(1);
    }
}
